const botao = document.querySelector("a");

botao.addEventListener("click", (event) => {
  const variavel = document.forms["form"]["variavel"].value;
  const valores = document.forms["form"]["valores"].value;
  
  if ((variavel == "") || (valores == "")) {
    alert("Insira os valores corretamente")
  } else {
    var val = JSON.parse("[" + valores + "]");
    document.getElementById("tb").innerHTML = criaTabela(variavel,val);
  }
})
//------------------------------------------------------------------------------
function criaTabela(variavel,valores){
  var matriz = new Array();
  var men = menor(valores);
  var mai = men + amplitude(valores);
  var xi = (men + mai)/2;
  var Ni = 0,Fi=0,Ci=0;
  var Nilinha = valores.length,
      Filinha = 1,
      Cilinha = 100;
  for(var i=0;i<linhas(valores);i++){
    matriz[i]=[men.toFixed(2)+" |- "+mai.toFixed(2),xi.toFixed(2),
    ni(men,mai,valores),Ni+=ni(men,mai,valores),Nilinha,
    (ni(men,mai,valores)/valores.length).toFixed(2),(Fi+=ni(men,mai,valores)/valores.length).toFixed(2),Filinha.toFixed(2),
    (ni(men,mai,valores)/valores.length)*100+"%",(Ci+=(ni(men,mai,valores)/valores.length))*100+"%",Cilinha+"%"];
    //Ni linha
    Nilinha-=ni(men,mai,valores);
    //Fi linha
    Filinha-=ni(men,mai,valores)/valores.length;
    //Cilinha
    Cilinha-=(ni(men,mai,valores)/valores.length)*100;
    //valores
    men = mai;
    mai += amplitude(valores);
    //xi
    xi = (men + mai)/2;
  }
  return tabela(matriz,variavel,valores);
}
function ni(men,mai,valores){
  var cont = 0;
  for(var i=0;i<valores.length;i++){
    if ((valores[i]>=men) && (valores[i]<=mai))
      cont++;
  }
  return cont;
}
function linhas(valores){
  return Math.sqrt(valores.length);
}
function amplitude(valores){
  var mai = maior(valores);
  var men = menor(valores);
  var dif = (mai - men)/linhas(valores);
  return dif;
}
function maior(valores){
  var maior = 0;
  for(var i=0;i<valores.length;i++){
    if(valores[i]>maior)
      maior = valores[i];
  }
  return maior;
}
function menor(valores){
  var menor = maior(valores);
  for(var i=0;i<valores.length;i++){
    if(valores[i]<menor)
      menor = valores[i];
  }
  return menor;
}
function tabela(matriz,variavel,valores){
//console.log(matriz[1][1]);
  var txt='';
  txt += "<tr><th>"+ variavel +"</th><th>xi</th><th>ni</th><th>Ni</th><th>Ni'</th><th>fi</th><th>Fi</th><th>Fi'</th><th>ci</th><th>Ci</th><th>Ci'</th></tr>";

  for(var i=0;i<linhas(valores);i++){
    txt += "<tr>";
    for(var j=0;j<11;j++){
      txt += "<th>" + matriz[i][j] + "</th>"; 
    } 
    txt += "</tr>";
  }
  return txt;
}
/*
altura
1.65,1.73,1.95,1.53,1.7,1.79,1.8,1.75,1.75,1.75,1.79,1.73,1.6,1.74,1.83,1.75,1.75,1.71,1.73,1.82
18,18,18,18,18,18,20,30,28,18,18,20,18,18,18,20,19,19,18,19

*/